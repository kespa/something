import ceramic.Scene;
import ceramic.Visual;
import ceramic.Quad;
import ceramic.Color;

using ceramic.VisualTransition;

class Transition extends Quad {
	var bars = new Array<Quad>();
	var barHeight: Float;

	@event public function done();

	public function new(fadein:Bool = true) {
		super();
		size(app.scenes.main.width, app.scenes.main.height);
		alpha = 0;
		depth = 100;

		barHeight = height/4; // height/x = x bars, or custom size

		for (i in 1...cast(height / barHeight) + 1) { // 35 is the height of each bar, +1(or x) makes it cover the screen because the barHeight isnt even with the screen height
			var odd = (i % 2 == 1 ? true : false); // if the pos is odd

			var q = new Quad();
			q.color = (odd ? 0x4C7BFA : 0xF51616);
			q.size(width, barHeight);
			q.anchor(0.5, 0);
			if (fadein) {
				q.pos((odd ? -q.width / 2 : width + q.width / 2), (i - 1) * barHeight);
			} else {
				q.pos(width / 2, (i - 1) * barHeight);
			}
			q.clip = this;

			var b = new ceramic.Border(); // the border for each block
			b.borderColor = Color.BLACK;
			b.depth = (odd ? 0 : -1);
			b.borderSize = 1.5;
			b.size(q.width, q.height);
			q.add(b);

			bars.push(q);
			add(q);
		}

		for (b in bars) {
			if (fadein) {
				b.transition(EXPO_EASE_IN_OUT, 1, (cb) -> {
					cb.x = width / 2;
				}).onComplete(this, () -> {
					emitDone();
				});
			} else {
				var odd = (bars.indexOf(b) % 2 == 1 ? true : false); // if the pos is odd
				b.transition(EXPO_EASE_IN_OUT, 1, (cb) -> {
					cb.x = (odd ? -b.width / 2 : width + b.width / 2);
				}).onComplete(this, () -> {
					emitDone();
				});
			}
		}
	}
	static public function fadeIn(s: Scene, ns: Scene) { //begin the animation
		var t = new Transition();
		s.add(t);
		t.onceDone(s, () -> {
			app.scenes.main = ns;
		});
	}
	static public function fadeOut(s: Scene) { //end the animation
		var t = new Transition(false);
        t.onceDone(s, () -> {t.destroy();});
        s.add(t);
	}
}