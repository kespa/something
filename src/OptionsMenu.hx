import ceramic.Scene;
import ceramic.Quad;
import ceramic.Text;
import ceramic.Color;
import ceramic.Border;
import ceramic.Visual;
import ceramic.Line;
import ceramic.PersistentData;

using ceramic.VisualTransition;

class OptionsMenu extends Scene {
	var bg:Background;

	override function preload() {
		assets.add(Images.IMAGES__ARROW); //back arrow
	}

	override function create() {
		bg = new Background(this);
		add(bg);

		var ba = new Quad();
		ba.texture = assets.texture(Images.IMAGES__ARROW);
		ba.anchor(0.5, 0.5);
		ba.scale(0.05);
		ba.pos((ba.width * ba.scaleX)/2 + 20, (ba.height/2 * ba.scaleY) + 20);
		ba.onPointerOver(this, (info) -> {
			ba.transition(ELASTIC_EASE_IN_OUT, 0.3, (cb) -> {
				cb.scale(0.07);
				cb.rotation = 360;
			});
		});
		ba.onPointerOut(this, (info) -> {
			ba.transition(ELASTIC_EASE_IN_OUT, 0.3, (cb) -> {
				cb.scale(0.05);
				cb.rotation = 0;
			});
		});
		ba.oncePointerDown(this, (info) -> {
			Transition.fadeIn(this, new MainMenu());
		});
		add(ba);

		var debug = new Checkbox("Debug Mode");
		debug.pos(width / 2, height * 0.5);
		add(debug);

		var fs = new Checkbox("Fullscreen", app.settings.fullscreen);
		fs.pos(width / 2, height * 0.6);	
		fs.onEnabled(this, () -> {
			app.settings.fullscreen = true;
		});
		fs.onDisabled(this, () -> {
			app.settings.fullscreen = false;
		});
		add(fs);
		Transition.fadeOut(this);
	}

	override function update(d:Float) {
		bg.update(d);
	}
}

class Checkbox extends Visual {
	var caption:Text;
	var cbox:Quad;
	public var enabled: Bool;
	@event public function enabled();
	@event public function disabled();

	public function new(c:String, on = false) {
		super();

		caption = new Text();
		caption.anchor(0, 0.5);
		caption.content = c;
		caption.color = Color.BLACK;
		add(caption);

		cbox = new Quad();
		cbox.color = Color.WHITE;
		cbox.size(30, 30);
		cbox.anchor(0, 0.5);

		var b = new Border();
		b.borderColor = Color.BLACK;
		b.borderSize = 2;
		b.size(cbox.width, cbox.height);
		cbox.add(b);

		anchor(0.5, 0.5);
		size(caption.width + cbox.width + 15, cbox.height);

		caption.pos(0, height / 2);
		cbox.pos(caption.width + 15, height / 2);
		add(cbox);

        cbox.onPointerDown(this, (info) -> {
            toggle();
        });

		enabled = on;
		toggle(); //the starting state
	}

	public function toggle() {
		if (enabled) {
			emitEnabled();
			var l1 = new Line();
			l1.points = [
				3, 3,
                cbox.width - 3, cbox.height - 3
			];
			l1.thickness = 2;
            l1.color = Color.BLACK;
			l1.id = "l1";
			cbox.add(l1);

			var l2 = new Line();
			l2.points = [
				cbox.width - 3, 3,
                3, cbox.height - 3
			];
			l2.thickness = 2;
            l2.color = Color.BLACK;
			l2.id = "l2";
			cbox.add(l2);
		} else {
			emitDisabled();

			if (cbox.childWithId("l1") != null && cbox.childWithId("l2") != null) {
				cbox.childWithId("l1").destroy();
            	cbox.childWithId("l2").destroy();
			}
        }

		enabled = !enabled;
	}
}