import ceramic.BitmapFont;
import ceramic.Text;
import ceramic.Scene;
import ceramic.Color;
import ceramic.Shape;
import ceramic.Line;

class MainMenu extends Scene {
    var bg: Background;
    var battleBtn: MenuButton;
    var optionBtn: MenuButton;
    var achieveBtn: MenuButton;
    var textSize = 45;
    override function preload() {}

    override function create() {
        bg = new Background(this);
        add(bg);

        battleBtn = new MenuButton(
        [
            5, 5, //top left
            width * 0.5, 5,
            width * 0.5, height * 0.55,
            width * 0.3, height - 5,
            5, height - 5 //bottom left
        ],
        "Throw Hands!");
        battleBtn.color = Color.RED;
        add(battleBtn);

        achieveBtn = new MenuButton(
        [
            5, 0,
            width * 0.5 - 5, 0,
            width * 0.5 - 5, height * 0.45 - 5,
            -(width * 0.5 - width * 0.3) + 5, height * 0.45 - 5      
        ],
        "Achievements");
        achieveBtn.pos(width * 0.5, height * 0.55); //on the bottom right
        achieveBtn.color = Color.LIME;
        achieveBtn.disable();
        add(achieveBtn);

        optionBtn = new MenuButton( //TODO add options screen
        [
            5, 5,
            width * 0.5 - 5, 5,
            width * 0.5 - 5, height * 0.55 - 5,
            5, height * 0.55 - 5
        ],
        "Options");
        optionBtn.pos(width * 0.5, 0);
        optionBtn.color = 0x426EFF;
        optionBtn.oncePointerDown(this, (info) -> {
            Transition.fadeIn(this, new OptionsMenu());
        });
        add(optionBtn);

        Transition.fadeOut(this);
    }
    override function update(d: Float) {
        bg.update(d);
    }
}

class MenuButton extends Shape {
    var text: Text;
    var bdr: Line; //the border
    public function new(pts: Array<Float>, label: String) {
        super();

        complexHit = true;
        alpha = 0.8;

        points = pts;
        text = new Text();
        text.content = label;
        text.pointSize = 40;
        text.color = Color.BLACK;
        text.anchor(0.5, 0.5);
        text.pos(width/2, height/2);
        add(text);

        bdr = new Line();
        bdr.points = points.concat([points[0], points[1], points[2], points[3]]);
        //bdr.loop = true;
        bdr.thickness = 3;
        bdr.color = Color.BLACK;
        add(bdr);

        bdr.alpha = 1;
        text.alpha = 0.9;

        onPointerOver(this, (info) -> {
            bdr.color = Color.YELLOW;
            bdr.thickness = 5;
            text.pointSize = 45;
        });
        onPointerOut(this, (info) -> {
            bdr.color = Color.BLACK;
            bdr.thickness = 3;
            text.pointSize = 40;
        });
    }
    public function disable() {
        alpha = 0.3;
        text.alpha = 0.3;
        offPointerOut();
        offPointerOver();
    }
    public function enable() {
        alpha = 0.7;
        bdr.alpha = 0.9;
        text.alpha = 0.9;
        onPointerOver(this, (info) -> {
            bdr.color = Color.YELLOW;
            bdr.thickness = 5;
            text.pointSize = 45;
        });
        onPointerOut(this, (info) -> {
            bdr.color = Color.BLACK;
            bdr.thickness = 2;
            text.pointSize = 40;
        });
    }
}